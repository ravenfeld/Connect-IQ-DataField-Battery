# Connect-IQ-DataField-Battery
![0](https://gitlab.com/ravenfeld/Connect-IQ-DataField-Battery/raw/develop/screenshot/0.png)
![0](https://gitlab.com/ravenfeld/Connect-IQ-DataField-Battery/raw/develop/screenshot/1.png)
![0](https://gitlab.com/ravenfeld/Connect-IQ-DataField-Battery/raw/develop/screenshot/2.png)

# Link
[Battery Level](https://apps.garmin.com/fr-FR/apps/690f1d61-6063-4bcc-93f8-e39c4a614f81)

[All projet](https://apps.garmin.com/fr-FR/developer/9a164185-3030-48d9-9aef-f5351abe70d8/apps)
